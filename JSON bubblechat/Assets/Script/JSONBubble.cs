﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Newtonsoft.Json;


public class JSONBubble : MonoBehaviour
{

    [SerializeField] private string jsonLink;

    [SerializeField] private TMP_Text nameText;
    [SerializeField] private TMP_Text emailText;

    [SerializeField] private int id;

    private void Start()
    {
        WWW json = new WWW(jsonLink);
        StartCoroutine(JSONToText(json));
    }

    IEnumerator JSONToText(WWW json)
    {
        yield return json;
        SetText(json);
        
    }

    private void SetText(WWW json){
        
        nameText.text = GetName(json.text);
        emailText.text = GetEmail(json.text);
    }
    private string GetName(string json)
    {
        List<JsonID> userID = JsonConvert.DeserializeObject<List<JsonID>>(json);
        return userID[id].name.ToString();
    }

    private string GetEmail(string json)
    {
        List<JsonID> userID = JsonConvert.DeserializeObject<List<JsonID>>(json);
        return userID[id].email.ToString();
    }

}
